package slider.millas;
public class Grafico extends javax.swing.JFrame {

    public Grafico() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sliderKm = new javax.swing.JSlider();
        etiqueta00 = new javax.swing.JLabel();
        etiquetaKm = new javax.swing.JLabel();
        etiquetaM = new javax.swing.JLabel();
        etiquetaMillasPorHora = new javax.swing.JLabel();
        sliderM = new javax.swing.JSlider();
        etiquetaKilometroPorHora = new javax.swing.JLabel();
        etiqueta0 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        sliderKm.setBackground(new java.awt.Color(255, 102, 51));
        sliderKm.setMaximum(600);
        sliderKm.setValue(300);
        sliderKm.setName("sliderKm"); // NOI18N
        sliderKm.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderKmStateChanged(evt);
            }
        });

        etiqueta00.setText("0");

        etiquetaKm.setText("300 kms / h");

        etiquetaM.setText("186 mph");

        etiquetaMillasPorHora.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        etiquetaMillasPorHora.setText("Millas por hora");

        sliderM.setBackground(new java.awt.Color(204, 204, 0));
        sliderM.setForeground(new java.awt.Color(0, 204, 0));
        sliderM.setMaximum(372);
        sliderM.setValue(186);
        sliderM.setName("sliderM"); // NOI18N
        sliderM.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderMStateChanged(evt);
            }
        });

        etiquetaKilometroPorHora.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        etiquetaKilometroPorHora.setText("Kilómetros por hora");

        etiqueta0.setText("0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(etiqueta00, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sliderM, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(etiquetaM))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(232, 232, 232)
                        .addComponent(etiquetaKilometroPorHora))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(72, 72, 72)
                                .addComponent(etiqueta0, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(sliderKm, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(251, 251, 251)
                                .addComponent(etiquetaMillasPorHora)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(etiquetaKm)))
                .addGap(50, 57, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(etiquetaKilometroPorHora)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(sliderKm, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(etiqueta0)
                        .addComponent(etiquetaKm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(etiquetaMillasPorHora)
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(etiquetaM)
                            .addComponent(sliderM, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(73, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(etiqueta00)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sliderKmStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderKmStateChanged
        int prueba = this.sliderKm.getValue();
        this.etiquetaKm.setText(prueba + " kms / h");
        int kmAMilla = (int)Math.floor(prueba / 1.609);
        this.etiquetaM.setText(kmAMilla + " mph");
        this.sliderM.setValue(kmAMilla);
    }//GEN-LAST:event_sliderKmStateChanged

    private void sliderMStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderMStateChanged
        int prueba = this.sliderM.getValue();
        this.etiquetaM.setText(prueba + " mph");
        int millaAKm = (int)Math.ceil(prueba * 1.609);
        this.etiquetaKm.setText(millaAKm + " kms / h");
        this.sliderKm.setValue(millaAKm);
    }//GEN-LAST:event_sliderMStateChanged

   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Grafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Grafico().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel etiqueta0;
    private javax.swing.JLabel etiqueta00;
    private javax.swing.JLabel etiquetaKilometroPorHora;
    private javax.swing.JLabel etiquetaKm;
    private javax.swing.JLabel etiquetaM;
    private javax.swing.JLabel etiquetaMillasPorHora;
    private javax.swing.JSlider sliderKm;
    private javax.swing.JSlider sliderM;
    // End of variables declaration//GEN-END:variables
}
